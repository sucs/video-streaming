#This script sets up an nginx rtmp server from scratch. If you don't know what you're doing, don't run this.

#the user to run the streaming server as
USERN="stream"
#hopefully you won't need to change this but who knows, security and whatnot
NGINXVER="1.9.6"

#check if I'm being run in the right place
ls $(pwd)/setup.sh &> /dev/null
if [ $? -ne 0 ]; then echo "Please run this script from its containing directory"; exit 1; fi

#chuck people away if they are idiots
if [ $(whoami) != $USERN ];	then echo "This script must be run as the user $USERN"
	exit 1
elif [ ! -f ./nginx.conf ];	then echo "Streaming has already been setup... or something is broken... Seek help."
	exit 1
fi
 
#okay, lets get on with it
git submodule init
git submodule update

if [ ! -f nginx-$NGINXVER.tar.gz ]
	then
	wget http://nginx.org/download/nginx-$NGINXVER.tar.gz
fi
tar -zxvf nginx-$NGINXVER.tar.gz
( cd nginx-$NGINXVER/ && ./configure --with-http_ssl_module --add-module=../nginx-rtmp-src && make )

echo "Turning on nginx with the RTMP module..."

mv ./nginx.conf ./nginx-$NGINXVER/conf
./nginx-$NGINXVER/objs/nginx -p ~/video-streaming/nginx-$NGINXVER

echo "Done!"
