echo "Make sure ssytem is up to date"
apt-get update
apt-get upgrade

echo "enable jessie-backports"
echo "deb http://http.debian.net/debian jessie-backports main" >>  /etc/apt/sources.list.d/backports.list

echo "Installing deps"
apt-get install -y libavutil-dev -t jessie-backports
apt-get install -y git cmake build-essential freeglut3-dev libav-tools libavcodec* libavutil-dev libavcodec-dev libavfilter-dev libavdevice-dev libswresample-dev libcurl4-openssl-dev zlib1g-dev libqt5widgets5 qtbase5-dev libqt5x11extras5-dev libxcb1-dev libxcb-xinerama0-dev libx264-dev freetype libfreetype6-dev libfontconfig1-dev xpra